//
//  AlgorithmsViewController.swift
//  Cypher
//
//  Created by John Kotz on 5/7/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

import Foundation

class AlgorithmsViewController: UITableViewController, UITableViewDelegate {
    var algorithms: Array<String> = []
    var selectedAlgorithmDict: Dictionary<String, AnyObject> = Dictionary()
    var algorithmsDict: Dictionary<String,(String, done: (String)->(), decrypt: Bool)->()> = Dictionary()
    
    override func viewDidLoad() {
        self.tableView.delegate = self
        
        // Make a encryptor object just to get the algorithms (*whisper* THIS IS BAD)
        self.algorithms = Encrypter(key: [], delegate: MainViewController(), textView: MainViewController().inputTextView).algorithms as Array<String>
        self.algorithmsDict = Encrypter(key: [], delegate: MainViewController(), textView: MainViewController().inputTextView).algorithmsDict
        
        if let algorithm: String = NSUserDefaults.standardUserDefaults().valueForKey("algorithm") as? String  {
            selectedAlgorithmDict["name"] = algorithm
            var description = "There is no description of thise algorithm yet"
            let dict = NSDictionary(contentsOfFile: "AlgorithmsDescription.plist")! as Dictionary
            if let savedDecription = dict[algorithm] as? String {
                description = savedDecription
            }
            selectedAlgorithmDict["description"] = description
            if let index = find(self.algorithms, algorithm){
                selectedAlgorithmDict["index"] = index
            }
        }
        
        var revealController: SWRevealViewController = self.revealViewController()
        self.title = "Algorithms"
        
        var revealButtonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "reveal-icon.png"), style: UIBarButtonItemStyle.Plain, target: revealController, action: Selector("revealToggle:"))
        
        self.navigationItem.leftBarButtonItem = revealButtonItem
    }
    
    override func viewWillAppear(animated: Bool) {
        if let algorithm: String = NSUserDefaults.standardUserDefaults().valueForKey("algorithm") as? String  {
            // I was actually smart with this kinda thing. If let things are great for this!!
            let indexPath = NSIndexPath(forRow: find(algorithms, algorithm)!, inSection: 0)
            tableView.selectRowAtIndexPath(indexPath, animated: animated, scrollPosition: UITableViewScrollPosition.Top)
            
            tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: indexPath.row + 1, inSection: indexPath.row)], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let name = selectedAlgorithmDict["name"] as? String {
            return algorithms.count + 1
        }
        return algorithms.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if let funct = self.algorithmsDict[self.algorithms[indexPath.row]] {
            let algorithm = algorithms[indexPath.row]
            // Save it!!
            NSUserDefaults.standardUserDefaults().setValue(algorithm, forKey: "algorithm")
            NSUserDefaults.standardUserDefaults().synchronize()
        }else{
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            if let algorithm = NSUserDefaults.standardUserDefaults().valueForKey("algorithm") as? String {
                var index = find(self.algorithms, algorithm)
                if (index != -1) {
                    tableView.selectRowAtIndexPath(NSIndexPath(forRow: index!, inSection: 0), animated: true, scrollPosition: UITableViewScrollPosition.None)
                }
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: cellIdentifier)
        if var cell1: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell {
            cell = cell1
        }else{
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: cellIdentifier)
        }
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        if (self.algorithmsDict[algorithms[indexPath.row]] == nil) {
            // This actually looks good.
            cell.detailTextLabel?.text = "Not implemented!"
            cell.detailTextLabel?.textColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.9)
        }
        
        var adder = 0
        if let indexOfSelectedAlgorithm = selectedAlgorithmDict["index"] as? Int {
            if indexOfSelectedAlgorithm == indexPath.row - 1 {
                
            }
        }
        
        cell.textLabel?.text = algorithms[indexPath.row + adder]
        
        return cell
    }
}