//
//  Encrypter.swift
//  Cypher
//
//  Created by John Kotz on 5/9/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

import Foundation


// Alphabet converters
func alphabetFromStringToArray(alphabet: String) -> Array<Character> {
    var output: Array<Character> = []
    for char: Character in alphabet {
        output.append(char)
    }
    return output
}

func alphabetFromArrayToDict(alphabet: Array<Character>) -> Dictionary<Character, Int> {
    var output: Dictionary<Character, Int> = Dictionary()
    var i = 0
    for char: Character in alphabet {
        output[char] = i
        i++
    }
    return output
}

func alphabetFromStringToDict(alphabet: String) -> Dictionary<Character, Int> {
    var output: Dictionary<Character, Int> = Dictionary()
    var i = 0
    for char: Character in alphabet {
        output[char] = i
        i++
    }
    return output
}

// Alphabet generators/changers

func rotateAlpha(alphabet: Array<Character>, byNumberOfCharacters number: UInt32) -> Array<Character> {
    let part1 = Array(alphabet[0...Int(number)%alphabet.count - 1])
    let part2 = Array(alphabet[Int(number)%alphabet.count...alphabet.count-1])
    return part2 + part1
}

func randomAlpha(alphabet: Array<Character>, key: UInt32) -> Array<Character> {
    srand(key)
    var outputAlpha: Array<Character> = []
    for (var i = 0; i < alphabet.count; i++) {
        var index = Int(rand())%(alphabet.count)
        while (contains(outputAlpha, alphabet[index])) {
            index = (index + 1)%(alphabet.count)
        }
        outputAlpha.append(alphabet[index])
    }
    return outputAlpha
}

// Translators

func translate(input: String, alphabetFrom: Array<Character>, alphabetTo: Array<Character>, update: (String, String, Int, Int?) -> ()) -> String {
    var output: String = ""
    var i = 0
    for char:Character in input {
        if let index = find(alphabetFrom, char) {
            output = output + "\(alphabetTo[index])"
        }else{
            output = output + "¿"
        }
        update(input, output, i, nil)
        if (NSUserDefaults.standardUserDefaults().boolForKey("progressMode")) {usleep(200)}
        i++
    }
    return output
}


class Encrypter {
    var keys:Array<String> = []
    
    // Algorithms
    let algorithms = ["Ceasar Cypher", "Randomized Letter Cypher", "Split Key Cypher"]
    var algorithmsDict: Dictionary<String,(String, done: (String)->(), decrypt: Bool)->()>
    
    // Alphabet
    let stringAlphabet = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()\"'{}[];:,.<>/?+_-=`~ "
    let alphabet:Array<Character>
    let alphaDict: Dictionary<Character, Int>
    
    // Delegate resources
    let delegate: UIViewController
    let textView: UITextView
    var progressView: YLProgressBar = YLProgressBar()
    var useProgressView = false
    var lastResult = ""
    
    // Progress Indication
    var lastProgress = 0
    
    init(key: Array<String>, delegate: UIViewController, textView: UITextView) {
        self.keys = key
        self.alphabet = alphabetFromStringToArray(stringAlphabet)
        self.alphaDict = alphabetFromArrayToDict(self.alphabet)
        self.delegate = delegate
        self.textView = textView
        
        // This dictionary contains all the encryption algorithms and their functions
        // Good for
        self.algorithmsDict = Dictionary()
        self.algorithmsDict["Ceasar Cypher"] = self.encryptCeasar
        self.algorithmsDict["Randomized Letter Cypher"] = self.encryptRandomLetterCypher
    }
    
    func encryptUsingMethod(method: String, input: String, done: (String) -> (), decrypt: Bool) {
        if (find(algorithms, method) == -1) {
            // This will likely never be called, unless something sets an algorithm that isn't here
            fatalError("Method not allowed: \(method)")
        }else{
            // Check if the function is ready
            if let function = self.algorithmsDict[method] {
                // Run the function
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    function(input, done: done, decrypt: decrypt)
                })
            }else{
                // Function not implemented
                println("No dice! That algorithm (\(method)) has not been implemented yet!")
                done(input)
                self.done()
            }
        }
    }
    
    func setProgressView(progressView: YLProgressBar) {
        // This is for setting the progress view. By default, it is not used,
        // but this will set it and allow it to be used
        self.progressView = progressView
        self.useProgressView = true
    }
    
    func encryptRandomLetterCypher(input: String, done: (String)->(), decrypt: Bool) {
        // Go forth and do my bidding!!
        var result = ""
        let key = self.getFullKey()
        if (!decrypt) {
            // AKA: encrypt
            result = translate(input, self.alphabet, randomAlpha(self.alphabet, key), self.generalUpdate)
        }else{
            // Just reverse the translater! Simple
            result = translate(input, randomAlpha(self.alphabet, key), self.alphabet, self.generalUpdate)
        }
        dispatch_async(dispatch_get_main_queue(), {
            // Beam me up, Scotty!
            done(result)
            self.done()
        })
        self.lastResult = result
    }
    
    func getFullKey() -> UInt32 {
        var key: String = ""
        for key1: String in self.keys {
            key = key + key1
        }
        let doneKey: Int = abs(key.hash)%Int(UInt32.max)
        return UInt32(doneKey)
    }
    
    func encryptSplitKey(input: String, done: (String)->(), decrypt: Bool) {
        var array: Array<Int> = []
        let key = self.getFullKey()
        let stringKey = "\(key)"
        let keyLength: Int = count(stringKey) / 3
        
        // For my next act, I will create three different keys... From one!!!
        array.append(stringKey.substringWithRange(Range<String.Index>(start: stringKey.startIndex, end: advance(stringKey.startIndex, keyLength))).toInt()!)
        array.append(stringKey.substringWithRange(Range<String.Index>(start: advance(stringKey.startIndex, keyLength), end: advance(stringKey.startIndex, keyLength * 2))).toInt()!)
        array.append(stringKey.substringWithRange(Range<String.Index>(start: advance(stringKey.startIndex, keyLength * 2), end: stringKey.endIndex)).toInt()!)
        
        for (var i = 0; i < array.count; i++) {
            let key: Int = array[i]
            srand48(key)
            
            
        }
    }
    
    func encryptCeasar(input: String, done: (String)->(), decrypt: Bool) {
        // Get me a the best core there is!!
        var result = ""
        let key = self.getFullKey()
        if (!decrypt) {
            // If you don't know that !decrypt means encrypt by now, there is no point in continuing to read these
            result = translate(input, self.alphabet, rotateAlpha(self.alphabet, byNumberOfCharacters: key), self.generalUpdate)
        }else{
            // More reversing...
            result = translate(input, rotateAlpha(self.alphabet, byNumberOfCharacters: key), self.alphabet,self.generalUpdate)
        }
        dispatch_async(dispatch_get_main_queue(), {
            // How do you like them apples!
            done(result)
            self.done()
        })
        self.lastResult = result
    }
    
    func done() {
        self.progressView.setProgress(1.0, animated: false)
    }
    
    func generalUpdate(input: String, completed: String, progress: Int, outOf: Int?) {
        // Can I update the UI (depends on if progress mode is allowed and how many characters have been completed)
        let shouldUpdate = progress - self.lastProgress == 30 && NSUserDefaults.standardUserDefaults().boolForKey("progressMode")
        
        // Go tell it on the mountain
        dispatch_async(dispatch_get_main_queue(), {
            if (shouldUpdate && self.delegate.isViewLoaded() && self.delegate.view.window != nil) {
                self.textView.text = completed // I am showing the stuff that I have finished
                
                // Tell it to scroll
                let range:NSRange = NSMakeRange(count(self.textView.text) - 1, 1)
                self.textView.scrollRangeToVisible(range)
                
                if (self.useProgressView) {
                    // I will now blow your mind with a cool progress viewer. !*!*!*!*!*!
                    self.progressView.hidden = false
                    self.progressView.setProgress(CGFloat(progress) / CGFloat(count(input)), animated: false)
                }
            }
        })
        if (shouldUpdate) {
            // I just updated! Remember that!
            lastProgress = progress
        }
    }
}