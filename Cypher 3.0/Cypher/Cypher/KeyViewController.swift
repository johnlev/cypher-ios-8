//
//  KeyViewController.swift
//  Cypher
//
//  Created by John Kotz on 5/14/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//  selectedKeyProfile

import Foundation

class KeyViewController: UITableViewController, UITableViewDelegate {
    var keys: Array<String> = []
    var selectedKeys: Array<String> = []
    
    override func viewWillAppear(animated: Bool) {
        if let keys: Array<String> = NSUserDefaults.standardUserDefaults().valueForKey("keys") as? Array<String> {
            self.keys = keys
        }
        if let selectedKeys: Array<String> = NSUserDefaults.standardUserDefaults().valueForKey("selectedKeyProfile") as? Array<String> {
            self.selectedKeys = selectedKeys
        }
        
        var addBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: Selector("addKey"))
        self.navigationItem.rightBarButtonItem = addBarButton
        
        var revealButtonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "reveal-icon.png"), style: UIBarButtonItemStyle.Plain, target: self.revealViewController(), action: Selector("revealToggle:"))
        
        self.navigationItem.leftBarButtonItem = revealButtonItem
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.keys.count
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            (tableView.cellForRowAtIndexPath(indexPath) as! EditableCell).editTextFeild.resignFirstResponder()
            if let i = find(self.selectedKeys, self.keys[indexPath.row]) {
                self.selectedKeys.removeAtIndex(i)
            }
            self.keys.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            NSUserDefaults.standardUserDefaults().setValue(self.keys, forKey: "keys")
            NSUserDefaults.standardUserDefaults().setValue(self.selectedKeys, forKey: "selectedKeyProfile")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func addKey() {
        self.keys.insert("", atIndex: 0)
        tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Left)
        (tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! EditableCell).editTextFeild.becomeFirstResponder()
    }
    
    func editingDoneForCell(cell: EditableCell) {
        let index = self.tableView.indexPathForCell(cell)!.row
        self.keys[index] = cell.editTextFeild.text
        NSUserDefaults.standardUserDefaults().setValue(self.keys, forKey: "keys")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if let i = find(self.selectedKeys, keys[indexPath.row]) {
            self.selectedKeys.removeAtIndex(i)
            cell?.accessoryType = UITableViewCellAccessoryType.None
        }else{
            self.selectedKeys.append(self.keys[indexPath.row])
            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        NSUserDefaults.standardUserDefaults().setValue(self.selectedKeys, forKey: "selectedKeyProfile")
        NSUserDefaults.standardUserDefaults().synchronize()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell: EditableCell = EditableCell(initialText: self.keys[indexPath.row], backgroundText: "Type key", delegateFunction: editingDoneForCell)
        if var cell1: EditableCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? EditableCell {
            cell = cell1
        }else{
            cell = EditableCell(initialText: self.keys[indexPath.row], backgroundText: "Type key", delegateFunction: editingDoneForCell)
            
        }
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.editTextFeild.text = keys[indexPath.row]
        var i = find(self.selectedKeys, self.keys[indexPath.row])
        if i != -1 && i != nil {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        return cell
    }
}