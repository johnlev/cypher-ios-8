//
//  EditableCell.swift
//  Cypher
//
//  Created by John Kotz on 5/14/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

import Foundation

class EditableCell: UITableViewCell, UITextFieldDelegate {
    var editTextFeild: UITextField
    var backgroundText: String = ""
    var textValue: String = ""
    var delegateFunction: (EditableCell) -> ()
    
    init(initialText: String?, backgroundText: String?, delegateFunction: (EditableCell) -> ()) {
        self.editTextFeild = UITextField()
        self.delegateFunction = delegateFunction
        if let bckgrdtxt = backgroundText {
            self.backgroundText = bckgrdtxt
        }
        if let intltxt = initialText {
            self.textValue = intltxt
        }
        super.init(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        self.editTextFeild.frame = CGRect(x: 15, y: 0, width: self.frame.width*2/3, height: self.frame.height)
        self.editTextFeild.delegate = self
        self.editTextFeild.adjustsFontSizeToFitWidth = true
        self.addSubview(editTextFeild)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.delegateFunction(self)
        return false
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}