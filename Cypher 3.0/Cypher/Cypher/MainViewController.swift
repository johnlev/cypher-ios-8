//
//  MainViewController.swift
//  Cypher
//
//  Created by John Kotz on 4/28/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

// HEHEHEE
let sampleText = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."

import Foundation
import UIKit

class MainViewController: UIViewController, UITextViewDelegate, UIAlertViewDelegate {
    var inputTextView: UITextView = UITextView()
    var goButton: UIButton = UIButton()
    var decryptButton: UIButton = UIButton()
    var progressView: YLProgressBar = YLProgressBar()
    var gesture: UITapGestureRecognizer = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.title = "Encrypt/Decrypt"
        
        var revealController: SWRevealViewController = self.revealViewController()
        
        revealController.panGestureRecognizer()
        revealController.tapGestureRecognizer()
        
        if (UIDevice.currentDevice().model.rangeOfString("iPad") != nil) {
            inputTextView.font = UIFont(name: "Arial", size: 16)
        }
        
        inputTextView.text = sampleText
        inputTextView.delegate = self
        inputTextView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        
        
        goButton.setImage(UIImage(named: "goButton.png"), forState: .Normal)
        goButton.setImage(UIImage(named: "goButtonDark.png"), forState: .Highlighted)
        goButton.addTarget(self, action: Selector("encrypt"), forControlEvents: .TouchUpInside)
        
        decryptButton.setImage(UIImage(named: "goButton-decrypt.png"), forState: .Normal)
        decryptButton.setImage(UIImage(named: "goButtonDark-decrypt.png"), forState: .Highlighted)
        decryptButton.addTarget(self, action: Selector("decrypt"), forControlEvents: .TouchUpInside)
        
        progressView.setProgress(0.0, animated: false)
        progressView.frame = CGRect(x: 0, y: inputTextView.frame.height, width: self.view.frame.width, height: 50)
        progressView.type = YLProgressBarType.Flat
        progressView.progressTintColor = UIColor(red: 0.1, green: 1.0, blue: 0.1, alpha: 1.0)
        progressView.hideStripes = true
        progressView.hideGloss = true
        
        gesture = UITapGestureRecognizer(target: self, action: Selector("tapOut"))
        
        self.view.addSubview(inputTextView)
        self.view.addSubview(goButton)
        self.view.addSubview(decryptButton)
        self.view.addSubview(progressView)
        self.view.addGestureRecognizer(gesture)
        
        var revealButtonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "reveal-icon.png"), style: UIBarButtonItemStyle.Plain, target: revealController, action: Selector("revealToggle:"))
        
        self.navigationItem.leftBarButtonItem = revealButtonItem
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
    }
    
    func encrypt() {
        go(false)
    }
    
    func decrypt() {
        go(true)
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.revealViewController().setFrontViewController(UINavigationController(rootViewController: KeyViewController()), animated: true)
        }
    }
    
    func go(decrypt: Bool) {
        var key: Array<String> = []
        var goAhead = true
        let keys = NSUserDefaults.standardUserDefaults().valueForKey("keys") as! Array<String>
        if let key1 = NSUserDefaults.standardUserDefaults().valueForKey("selectedKeyProfile") as? Array<String> {
            if key1.count == 0 {
                let alert = UIAlertView(title: "No Key!!", message: "You have no key to encrypt with. Please add one in the keys section of the settings", delegate: self, cancelButtonTitle: "Okay", otherButtonTitles: "Go there!")
                goAhead = false
                alert.show()
            }else{
                key = key1
                var i = 0
                while (i < key.count) {
                    let key2 = key[i]
                    let index = find(keys, key2)
                    if index == -1 {
                        key.removeAtIndex(i)
                        i--
                    }
                    i++
                }
            }
        }else{
            NSUserDefaults.standardUserDefaults().setValue(["default"], forKey: "selectedKeyProfile")
            key = ["default"]
        }
        if goAhead {
            self.inputTextView.editable = false
            self.goButton.enabled = false
            self.decryptButton.enabled = false
            self.progressView.hidden = false
        
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "processRunning")
            let encrypt:Encrypter = Encrypter(key: key, delegate: self, textView: self.inputTextView)
            encrypt.setProgressView(self.progressView)
            encrypt.encryptUsingMethod(NSUserDefaults.standardUserDefaults().valueForKey("algorithm") as! String, input: self.inputTextView.text, done: self.processDone, decrypt: decrypt)
        }
    }
    
    func processDone(output: String) {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "processRunning")
        self.inputTextView.editable = true
        self.goButton.enabled = true
        self.progressView.hidden = true
        self.decryptButton.enabled = true
        self.inputTextView.text = output
    }
    
    func tapOut() {
        inputTextView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        var hideButtonItem: UIBarButtonItem = UIBarButtonItem(title: "Hide", style: UIBarButtonItemStyle.Done, target: textView, action: Selector("resignFirstResponder"))
        
        self.navigationItem.rightBarButtonItem = hideButtonItem
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") { textView.resignFirstResponder(); return false }
        return true
    }
    
    override func viewDidLayoutSubviews() {
        inputTextView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height / 2)
        goButton.frame = CGRect(x: self.view.frame.width / 2, y: self.inputTextView.frame.origin.y + self.inputTextView.frame.height + 10, width: self.view.frame.width / 4, height: self.view.frame.width / 4)
        decryptButton.frame = CGRect(x: self.view.frame.width / 2 - self.view.frame.width / 4, y: self.inputTextView.frame.origin.y + self.inputTextView.frame.height + 10, width: self.view.frame.width / 4, height: self.view.frame.width / 4)
        
        progressView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:
            self.navigationController!.navigationBar.frame.height + UIApplication.sharedApplication().statusBarFrame.size.height)
    }
    
    func showSideBar(sender: AnyObject) {
        self.revealViewController().revealToggle(sender)
    }
}