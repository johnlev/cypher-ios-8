//
//  ThemeChooserViewController.swift
//  Cypher
//
//  Created by John Kotz on 5/8/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

import Foundation

class ThemeChooserViewController: UITableViewController, UITableViewDelegate {
    override func viewDidLoad() {
        // LITERALLY NOTHING YET
        self.tableView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.title = "Theme - under construction"
    }
}