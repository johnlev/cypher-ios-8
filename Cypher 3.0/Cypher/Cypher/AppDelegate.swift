//
//  AppDelegate.swift
//  Cypher
//
//  Created by John Kotz on 4/28/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SWRevealViewControllerDelegate {

    var window: UIWindow?
    var viewController = UIViewController()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "processRunning")
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "inputSave")
        if (NSUserDefaults.standardUserDefaults().valueForKey("launchedBefore") == nil) {
            NSUserDefaults.standardUserDefaults().setValue(["default"], forKey: "keys")
            NSUserDefaults.standardUserDefaults().setValue(["default"], forKey: "selectedKeyProfile")
            NSUserDefaults.standardUserDefaults().setValue("Ceasar Cypher", forKey: "algorithm")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
        }
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "key")
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        var sideViewController = SideViewController()
        var mainViewController = MainViewController()
        
        var mainNavController = UINavigationController(rootViewController: mainViewController)
        var sideNavController = UINavigationController(rootViewController: sideViewController)
        
        var revealController = SWRevealViewController(rearViewController: sideViewController, frontViewController: UINavigationController(rootViewController: mainViewController))
        revealController.delegate = self
        
        revealController.bounceBackOnOverdraw = false
        
        self.viewController = revealController
        
        self.window?.rootViewController = self.viewController
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func revealController(revealController: SWRevealViewController!, animationControllerForOperation operation: SWRevealControllerOperation, fromViewController fromVC: UIViewController!, toViewController toVC: UIViewController!) -> UIViewControllerAnimatedTransitioning! {
        
//        if (operation !== SWRevealControllerOperationReplaceRightController) {
//            return nil
//        }
//
//        if (toVC.isKindOfClass())
        return nil
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

