//
//  InterfaceViewController.swift
//  Cypher
//
//  Created by John Kotz on 5/8/15.
//  Copyright (c) 2015 Lordtechy Productions. All rights reserved.
//

import Foundation

class InterfaceViewController: UITableViewController, UITableViewDelegate {
    // Each section is an array (not the best, but it works for now)
    let theme = ["Theme"]
    let encryptionInterface = ["Auto-copy", "Progress Mode", "No Sleep While Encrypting"]
    
    // Remember these. I don't know why I needed all this, but it works
    var switches: Array<UISwitch> = []
    var switchesHeight: Array<CGFloat> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = UITableView(frame: self.tableView.frame, style:UITableViewStyle.Grouped)
        tableView.scrollEnabled = false
        self.title = "Interface Settings"
        
        var revealButtonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "reveal-icon.png"), style: UIBarButtonItemStyle.Plain, target: self.revealViewController(), action: Selector("revealToggle:"))
        
        self.navigationItem.leftBarButtonItem = revealButtonItem
        
        // call the function when rotated
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // My functions
    
    func autoCopyValueChanged() {
        NSUserDefaults.standardUserDefaults().setBool(switches[0].on, forKey: "autoCopy")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func progressModeValueChanged() {
        NSUserDefaults.standardUserDefaults().setBool(switches[1].on, forKey: "progressMode")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func sleepValueChanged() {
        NSUserDefaults.standardUserDefaults().setBool(switches[2].on, forKey: "sleep")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    // Other boring functions
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return theme.count
        }else{
            return encryptionInterface.count
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Theme"
        }else{
            return "Encryption Interface"
        }
    }
    
    func rotated() {
        // I remove all from view and DELETE!
        for switcher: UISwitch in switches {
            switcher.removeFromSuperview()
            switchesHeight.append(switcher.frame.origin.y)
        }
        switches = []
        // Then I add them back in
        self.tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell: UITableViewCell = UITableViewCell()
        if var cell1: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell {
            cell = cell1
        }else{
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        var text = ""
        
        if (indexPath.section == 0) {
            text = theme[indexPath.row]
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }else{
            text = encryptionInterface[indexPath.row]
            var switcher = UISwitch()
            switches.append(switcher)
            
            
            // A hodge-podge of unrecognizable text that littlerally sets up the selector with the right value
            var selector = ""
            if (indexPath.row == 0) {
                if let value: Bool = NSUserDefaults.standardUserDefaults().valueForKey("autoCopy") as? Bool {
                    switcher.setOn(value, animated: false)
                }
                selector = "autoCopyValueChanged"
            }else if (indexPath.row == 1) {
                if let value: Bool = NSUserDefaults.standardUserDefaults().valueForKey("progressMode") as? Bool {
                    switcher.setOn(value, animated: false)
                }
                selector = "progressModeValueChanged"
            }else{
                if let value: Bool = NSUserDefaults.standardUserDefaults().valueForKey("sleep") as? Bool {
                    switcher.setOn(value, animated: false)
                }
                selector = "sleepValueChanged"
            }
            
            switcher.addTarget(self, action: Selector(selector), forControlEvents: UIControlEvents.ValueChanged)
            
            // Place it. I don't even want to talk about the hack that I used to do this
            var y = cell.frame.origin.y + cell.frame.size.height / 2 - switcher.frame.size.height / 2
            if (switchesHeight != []) {
                y = switchesHeight[indexPath.row]
            }
            
            switcher.frame = CGRect(x: self.view.frame.width - switcher.frame.size.width - 10, y: y, width: switcher.frame.size.width, height: switcher.frame.size.height)
            
            // Add the flour
            cell.addSubview(switcher)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
        }
        // We can't have a cell without text
        cell.textLabel?.text = text
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == 0 && indexPath.row == 0) {
            // Go to the useless view of theme. Who's job was that...
            self.navigationController?.pushViewController(ThemeChooserViewController(), animated: true)
            // Oh, that was my job :P
        }
    }
}