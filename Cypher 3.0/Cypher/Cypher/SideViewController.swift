//
//  SideViewController.swift
//  Cypher
//
//  Created by John Kotz on 4/28/15.
//  Copyright (c) 2015 Lordtechy `oductions. All rights reserved.
//

import Foundation
import UIKit

class SideViewController: UITableViewController, UITableViewDelegate, UIAlertViewDelegate {
    var tableview = UITableView()
    let sections = 2
    let settingsObjects = ["Algorithms", "Key", "Interface"]
    let mainObjects = ["Encrypt/Decrypt"]
    var mainViewController: MainViewController = MainViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView = UITableView(frame: self.tableView.frame, style:UITableViewStyle.Grouped)
        self.revealViewController().setFrontViewController(UINavigationController(rootViewController:mainViewController), animated: false)
        self.tableView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
    }
    
    override func viewDidAppear(animated: Bool) {
        var controllers = (self.revealViewController().frontViewController as! UINavigationController).viewControllers
        var current: AnyObject = controllers[0]
        if let controller = current as? AlgorithmsViewController {
            self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1), animated: animated, scrollPosition: UITableViewScrollPosition.Top)
        }else if let controller = current as? MainViewController {
            self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), animated: animated, scrollPosition: UITableViewScrollPosition.Top)
        }else if let controller = current as? InterfaceViewController {
            self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 1), animated: animated, scrollPosition: UITableViewScrollPosition.Top)
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return"Main"
        }else{
            return "Settings"
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return mainObjects.count
        }else{
            return settingsObjects.count
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell: UITableViewCell = UITableViewCell()
        if var cell1: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell {
            cell = cell1
        }else{
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        var text = ""
        if (indexPath.section == 0) {
            text = mainObjects[indexPath.row]
        }else{
            text = settingsObjects[indexPath.row]
        }
        
        cell.textLabel?.text = text
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var controllers = (self.revealViewController().frontViewController as! UINavigationController).viewControllers
        var current: AnyObject = controllers[0]
        if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                if !(current is AlgorithmsViewController) {
                    self.revealViewController().setFrontViewController(UINavigationController(rootViewController:AlgorithmsViewController()), animated: true)
                }
            }else if (indexPath.row == 1) {
                if !(current is KeyViewController) {
                    self.revealViewController().setFrontViewController(UINavigationController(rootViewController:KeyViewController(style: UITableViewStyle.Plain)), animated: true)
                }
            }else if (indexPath.row == 2) {
                if !(current is InterfaceViewController) {
                    self.revealViewController().setFrontViewController(UINavigationController(rootViewController: InterfaceViewController()), animated: true)
                }
            }
        }else {
            if (indexPath.row == 0) {
                if !(current is MainViewController) {
                    self.revealViewController().setFrontViewController(UINavigationController(rootViewController:mainViewController), animated: true)
                }
            }
        }
        self.revealViewController().revealToggle(nil)
    }
}