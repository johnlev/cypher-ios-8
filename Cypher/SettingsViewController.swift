//
//  SettingsViewController.swift
//  Cypher
//
//  Created by John Kotz on 7/31/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import UIKit
import Foundation

class SettingsViewController: UITableViewController, UIAlertViewDelegate  {
    @IBOutlet var table : UITableView!
    @IBOutlet var keyTextField : UITextField!
    @IBOutlet var passwordTextField : UITextField!
    @IBOutlet var keyLabel : UILabel!
    @IBOutlet var keyTableCell : UITableViewCell!
    @IBOutlet var progressSwitch : UISwitch!
    @IBOutlet var autoCopy : UISwitch!
    @IBOutlet weak var passwordSetInd: UILabel!
    @IBOutlet weak var progressModeOptionsButton: UIButton!
    
    
    let colorDict: Dictionary<String, UIColor> = dataObject().colorDict
    
    var backgroundColors = []
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var startingBackColor = ""
    var wrongPassword = false
    
    override func viewDidLoad() {
        
        if (!contains(defaults.dictionaryRepresentation().keys, "key")) {
            defaults.setValue(true, forKey: "key")
        }
        startingBackColor = String(format: defaults.objectForKey("backgroundColor") as NSString)
        
        if (defaults.objectForKey("key") == nil) {
            defaults.setValue("default", forKey: "key")
        }
        
        let key: String = String(format: defaults.objectForKey("key") as NSString)
        let progressKey = "progressMode"
        
        if (!contains(defaults.dictionaryRepresentation().keys, progressKey)) {
            defaults.setValue(true, forKey: progressKey)
        }
        
        if (defaults.objectForKey(progressKey) == nil) {
            defaults.setValue(true, forKey: progressKey)
        }
        
        if ((defaults.objectForKey("password") as NSString).isEqualToString("")) {
            passwordSetInd.text = "Not Set"
        }else{
            passwordSetInd.text = "Set"
        }
        
        view.backgroundColor = colorDict[String(format: defaults.objectForKey("backgroundColor") as NSString)]
        
        progressSwitch.on = defaults.objectForKey("progressMode") as Bool
        autoCopy.on = defaults.objectForKey("autoCopy") as Bool
        
        if ((defaults.objectForKey("password") as NSString).isEqualToString("")) {
            keyLabel.text = key
        }else{
            keyLabel.text = ""
        }
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        var alert = UIAlertController(title: "Help: ", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
        
        var path = NSBundle.mainBundle().pathForResource("Help", ofType: "plist")
        var helpDict: Dictionary = NSDictionary(contentsOfFile: path!)!
        
        var name = ""
        
        // Help for
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                name = "Key"
            }
        }else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                name = "Progress Mode"
                
            }else if (indexPath.row == 1) {
                name = "Auto Copy"
                
            }else if (indexPath.row == 2) {
                name = "Background Color"
            }else if (indexPath.row == 3) {
                name = "Password"
            }
        }
        
        
        alert.title = alert.title! + name
        
        var help: Array = helpDict["\(name)"] as NSArray
        alert.message = "\(help[0])"
        
        if (help.count > 1) {
            alert.addAction(UIAlertAction(title: "More Info", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.performSegueWithIdentifier("moreInfo", sender: name)
            }))
        }
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        var defaults = NSUserDefaults.standardUserDefaults()
        view.backgroundColor = colorDict[String(format: defaults.objectForKey("backgroundColor") as NSString)]
    }
    
    func configurationTextField(textField: UITextField!)
    {
        
        if let tField = textField {
            
            self.keyTextField = textField!        //Save reference to the UITextField
            
            self.keyTextField.text = String(format: defaults.objectForKey("key") as NSString)
        }
    }
    
    func passwordConfigurationTextField(textField: UITextField!) {
        if let tField = textField {
            
            textField.secureTextEntry = true
            
            self.passwordTextField = textField!        //Save reference to the UITextField
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.setSelected(false, animated: false)
        if (indexPath.section == 0 && indexPath.row == 0) {
            editKey()
        }else if (indexPath.section == 1 && indexPath.row == 2) {
            performSegueWithIdentifier("backgroundColor", sender: nil)
        }else if (indexPath.section == 1 && indexPath.row == 3) {
            checkPassword(self.goPassword)
        }
    }
    
    @IBAction func autoCopy(sender : UISwitch) {
        defaults.setValue(sender.on, forKey: "autoCopy")
        defaults.synchronize()
    }
    
    @IBAction func progressModeChanged(sender : UISwitch) {
        defaults.setValue(sender.on, forKey: "progressMode")
        defaults.synchronize()
    }
    
    @IBAction func progressOptions(sender: AnyObject) {
        var alert = UIAlertController(title: "Options", message: "Options for the progress mode. Red is currently selected", preferredStyle: UIAlertControllerStyle.Alert)
        
        if (!(defaults.objectForKey("fullProgressMode") as Bool)) {
            alert.addAction(UIAlertAction(title: "Just output", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction)in
                let defaults = NSUserDefaults.standardUserDefaults()
                
                defaults.setValue(false, forKey: "fullProgressMode")
                defaults.synchronize()
            }))
            
            alert.addAction(UIAlertAction(title: "Output + unfinised input", style: UIAlertActionStyle.Default, handler: { (UIAlertAction)in
                let defaults = NSUserDefaults.standardUserDefaults()
                
                defaults.setValue(true, forKey: "fullProgressMode")
                defaults.synchronize()
            }))
        }else{
            alert.addAction(UIAlertAction(title: "Just output", style: UIAlertActionStyle.Default, handler: { (UIAlertAction)in
                let defaults = NSUserDefaults.standardUserDefaults()
                
                defaults.setValue(false, forKey: "fullProgressMode")
                defaults.synchronize()
            }))
            
            alert.addAction(UIAlertAction(title: "Output + unfinised input", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction)in
                let defaults = NSUserDefaults.standardUserDefaults()
                
                defaults.setValue(true, forKey: "fullProgressMode")
                defaults.synchronize()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func checkPassword(complete: () -> Void) {
        if (!(self.defaults.objectForKey("password") as NSString).isEqualToString("")){
            var alert = UIAlertController(title: "Current Password", message: "Type in the current password", preferredStyle: UIAlertControllerStyle.Alert)
            
            if (wrongPassword) {
                alert.message = "You typed in the wrong password. Try again"
            }
            self.wrongPassword = false
            
            alert.addTextFieldWithConfigurationHandler(passwordConfigurationTextField)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { (UIAlertAction)in
                let defaults = NSUserDefaults.standardUserDefaults()
                
                if ((defaults.objectForKey("password") as NSString).isEqualToString(self.passwordTextField.text)) {
                    complete()
                }else{
                    self.wrongPassword = true
                    self.checkPassword(complete)
                }
                
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            complete()
        }
    }
    
    func editPassword() {
        var alert = UIAlertController(title: "New Password", message: "Type in your new password", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler(passwordConfigurationTextField)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: { (UIAlertAction)in
            self.defaults.setValue(self.passwordTextField.text, forKey: "password")
            self.passwordSetInd.text = "Set"
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func goPassword() {
        var alert = UIAlertController(title: "Actions", message: "What would you like to do?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Set password", style: UIAlertActionStyle.Default, handler: { (UIAlertAction)in
            self.editPassword()
        }))
        
        
        if (!(self.defaults.objectForKey("password") as NSString).isEqualToString("")){
            alert.addAction(UIAlertAction(title: "Turn off password", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction)in
                self.defaults.setValue("", forKey: "password")
                self.defaults.synchronize()
                self.passwordSetInd.text = "Not Set"
            }))
        }
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func editKey() {
        self.checkPassword(editKeyChecked)
    }
    
    func editKeyChecked() {
        var alert = UIAlertController(title: "Key", message: "Input your key", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: { (UIAlertAction)in
            
            let defaults = NSUserDefaults.standardUserDefaults()
            
            defaults.setValue(self.keyTextField.text as NSString, forKey: "key")
            
            self.keyLabel.text = self.keyTextField.text
            
            defaults.synchronize()
            
        }))
        
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func back(sender : UIBarButtonItem) {
        if (!(defaults.objectForKey("backgroundColor") as NSString).isEqualToString(self.startingBackColor as NSString)) {
            dismissViewControllerAnimated(true, completion: self.presentingViewController?.loadView)
        }else{
            dismissViewControllerAnimated(true, completion: nil)
        }
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "moreInfo") {
            var vc:HelpViewController = segue.destinationViewController as HelpViewController
            
            vc.setSubject(String(format:sender as NSString))
        }
    }
    
    func save() {
        
    }
}