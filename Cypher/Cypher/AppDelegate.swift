//
//  AppDelegate.swift
//  Cypher
//
//  Created by John Kotz on 6/23/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary?) -> Bool {
        // Override point for customization after application launch.
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let dict: Dictionary = defaults.dictionaryRepresentation()
//        defaults.setValue("", forKey: "password")
//        defaults.setValue(false, forKey: "shouldAskForPassword")
        let keys = dict.keys
        let key = "hasLaunchedOnce"
        
        if (!contains(keys, key)) {
            
            defaults.setValue("default", forKey: "key")
            defaults.setValue(true, forKey: "progressMode")
            defaults.setValue(false, forKey: "autoCopy")
            defaults.setValue("Grey", forKey: "backgroundColor")
            defaults.setValue(false, forKey: "shouldAskForPassword")
            defaults.setValue("", forKey: "password")
            
            defaults.setValue(false, forKey: "fullProgressMode")
            
            defaults.setValue(true, forKey: "hasLaunchedOnce")
            defaults.synchronize()
        }
        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        var date = NSDate()
        let defaults = NSUserDefaults.standardUserDefaults()
        
        defaults.setValue(date, forKey: "leftApp")
        defaults.synchronize()
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        var oldDate: NSDate = defaults.objectForKey("leftApp") as NSDate
        var timeSince = oldDate.timeIntervalSinceNow
        
        var seconds: Int = 0 - Int(timeSince)
        var minutes: Int = Int(Float(seconds) / 60.0)
        
        if (true){
            defaults.setValue(true, forKey: "shouldAskForPassword")
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        let defaults = NSUserDefaults.standardUserDefaults()
        
        defaults.setValue(true, forKey: "shouldAskForPassword")
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

