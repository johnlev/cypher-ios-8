//
//  PasswordViewController.swift
//  Cypher
//
//  Created by John Kotz on 8/12/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import Foundation
import UIKit

class PasswordViewController: UIViewController {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var wrongPasswordLabel: UILabel!
    
    override func viewDidLoad() {
        passwordTextField.becomeFirstResponder()
    }
    
    @IBAction func done(sender: AnyObject) {
        checkPasssword()
    }
    
    func checkPasssword() {
        
        if (defaults.objectForKey("password") as NSString == passwordTextField.text) {
            defaults.setValue(false, forKey: "shouldAskForPassword")
            dismissViewControllerAnimated(true, completion: nil)
        }else{
            wrongPasswordLabel.text = "Wrong password, try again"
            wrongPasswordLabel.hidden = false
            passwordTextField.text = ""
        }
    }
}