//
//  Encryptor.swift
//  Cypher
//
//  Created by John Kotz on 6/23/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//
import UIKit
import Foundation

var past: Array<String> = [];

extension Array {
    func combine(separator: String) -> String{
        var str : String = ""
        for (idx, item) in enumerate(self) {
            str += "\(item)"
            if idx < self.count-1 {
                str += separator
            }
        }
        return str
    }
}


func stringToArray(input:String) -> Array<String> {
    // Takes a string and separates all the elements into an array
    var output = [String]()
    for char in input{
        output.append(String(char))
    }
    
    return output
}

func arrayToString(input:Array<String>) -> String{
    var output: String = ""
    
    for char in input{
        output += char
    }
    
    return output
}

func getAlphabetArray() -> Array<String> {
    return [" ", "!", "#", "$", "%", "&", "(", ")", "*", "+", "-", ".", ",", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "]", "^", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{", "|", "}", "~"]
}

func getAlphabetDictionary() -> Dictionary<String, Int> {
    return[" " : 0, "!" : 1, "#" : 2, "$" : 3, "%" : 4, "&" : 5, "(" : 6, ")" : 7, "*" : 8, "+" : 9, "-" : 10,
        "." : 11, "," : 12, "/" : 13, "0" : 14, "1" : 15, "2" : 16, "3" : 17, "4" : 18, "5" : 19, "6" : 20,
        "7" : 21, "8" : 22, "9" : 23, ":" : 24, ";" : 25, "<" : 26, "=" : 27, ">" : 28, "?" : 29, "@" : 30,
        "A" : 31, "B" : 32, "C" : 33, "D" : 34, "E" : 35, "F" : 36, "G" : 37, "H" : 38, "I" : 39, "J" : 40,
        "K" : 41, "L" : 42, "M" : 43, "N" : 44, "O" : 45, "P" : 46, "Q" : 47, "R" : 48, "S" : 49, "T" : 50,
        "U" : 51, "V" : 52, "W" : 53, "X" : 54, "Y" : 55, "Z" : 56, "[" : 57, "]" : 58, "^" : 59, "`" : 60,
        "a" : 61, "b" : 62, "c" : 63, "d" : 64, "e" : 65, "f" : 66, "g" : 67, "h" : 68, "i" : 69, "j" : 70,
        "k" : 71, "l" : 72, "m" : 73, "n" : 74, "o" : 75, "p" : 76, "q" : 77, "r" : 78, "s" : 79, "t" : 80,
        "u" : 81, "v" : 82, "w" : 83, "x" : 84, "y" : 85, "z" : 86, "{" : 87, "|" : 88, "}" : 89, "~" : 90]
}

func findLetter(letter: Character, alphabet: Array<String>) -> Int {
    // Finds the letter in the alphabet using linear search.
    // I would use a binary search to speed things up, but I need to be able to use this on unsorted lists
    
    //Min variable; It will change
    var min = 0
    
    // Start search
    while (min <= alphabet.count - 1) {
        
        if (alphabet[min] == String(letter)) {
            // The selected letter was found! Return it
            return min
        }
        min++
    }
    // Not found
    return -1
}

func rotateAlpha(alphabet: Array<String>, characters: Int) -> Array<String> {
    // Takes the real alphabet and rotates it
    
    // Get some constants
    let length: Int = countElements(alphabet)
    let rotate: Int = characters % length
    
    //
    let endingArray = alphabet[0...rotate - 1]
    let startingArray = alphabet[rotate...length - 1]
    
    let final: Array = Array(startingArray) + Array(endingArray)
    
    return final
}

func getRandomAlphabet(alpha: Array<String>, seed: Int) -> Array<String> {
    var final: Array<String> = []
    var numberOfCharacters = 0;
    srand(CUnsignedInt(seed))
    var New = true
    var index: Int = 0
    
    while (numberOfCharacters < alpha.count) {
        
        if (New) {
            index = Int(rand())%(alpha.count)
        }
        
        var character = Character(alpha[index])
        
        var result: Int = findLetter(character, final)
        if (result == -1) {
            final.append(alpha[index])
            numberOfCharacters++;
            New = true;
        }else{
            index = (index + 1)%(alpha.count);
            New = false;
        }
    }
    
    return final
}

func getPartOfString(message: String, index: Int) -> String {
    var output = ""
    var pos = 0
    
    for character in message {
        if pos >= index {
            output = output + String(character)
        }
        pos++
    }
    
    return output
}

class Encryptor {
    // Initialize variables
    var key = ""
    
    init(){
    }
    
    func setup(key: String) {
        self.key = key
    }
    
    func hash(input: String) -> Int {
        return input.hashValue
    }
    
    func getPast() -> Array<String> {
        return past;
    }
    
    func translate(message: String, alphabet: Array<String>, newAlphabet: Array<String>, delegate: ViewController) -> String {
        // Translates a message using the standard alphabet and the encryption
        
        dispatch_async(dispatch_get_main_queue(), {
            delegate.progressView.progress = 0.0
            delegate.goButton.enabled = false
            delegate.settingsButton.enabled = false
            delegate.cypherSegmented.enabled = false
            delegate.inputTextView.editable = false
            delegate.inputTextView.scrollEnabled = false
            delegate.segmented.enabled = false
        });
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var output = "";
        var pos = 0;
        var lettersNow = 0;
        var lettersTranslated = 0;
        var progressMode: Bool = defaults.objectForKey("progressMode") as Bool
        
        
        var lettersPerUpdate = (countElements(message) / 65)
        if (countElements(message) > 1000) {
            lettersPerUpdate = lettersPerUpdate * Int(2 * Float(countElements(message))/1000.0)
        }
        if (countElements(message) > 10000) {
            lettersPerUpdate = lettersPerUpdate * Int(2 * Float(countElements(message))/10000.0)
        }
        
        
        var startTime = NSDate();
        var characterMapping = Dictionary<String, Int>()
        
        for letter: Character in message {
            var index: Int;
            
            if let ind = characterMapping[String(letter)] {
                index = ind
            }else{
                index = findLetter(letter, alphabet);
                characterMapping[String(letter)] = index;
            }
            lettersNow++;
            
            if (index != -1){
                output = output + newAlphabet[index]
                if (lettersNow >= lettersPerUpdate && progressMode) {
                    var partOfString = getPartOfString(message, pos + 1)
                    dispatch_async(dispatch_get_main_queue(), {
                        var showText = output
                        if (defaults.objectForKey("fullProgressMode") as Bool) {
                            showText = output + partOfString
                        }
                        delegate.inputTextView.text = showText
                        delegate.progressView.progress = Float(lettersTranslated) / Float(countElements(message))
                        
                        delegate.progressLabel.hidden = false
                        delegate.progressLabel.text = "\(Int(Float(lettersTranslated) / Float(countElements(message)) * 100))%"
                    });
                    lettersNow = 0;
                }
            }
            pos++
            lettersTranslated++;
        }
        
        var endTime = NSDate();
        var secs: NSTimeInterval = endTime.timeIntervalSinceDate(startTime);
        var value = Float(lettersTranslated) / Float(secs);
        
        self.done(output, delegate: delegate)
        
        return output
    }
    
    func ceasar(input: String, decrypt: Bool, delegate: ViewController) {
        // A ceasar cypher
        // This encryption type translates the message using
        // an alphabet that has been rotated.
        if (delegate.past as NSNumber !== 0) {
            for (var i = 0; i < delegate.past; i++) {
                past.removeLast()
            }
        }
        
        if (past.count == 0) {
            past.append(input);
        }
        
        delegate.goButton.enabled = false
//        delegate.settingsButton.enabled = false
        
        var alpha: Array<String> = getAlphabetArray()
        
        var rotatedAlpha: Array<String> = rotateAlpha(alpha, abs(hash(self.key))%(getAlphabetArray().count))
        var translation = "";
        
        delegate.activity.stopAnimating();
        
        //Send it all off to be done somewhere
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            if decrypt {
                translation = self.translate(input, alphabet: rotatedAlpha, newAlphabet: alpha, delegate: delegate)
            }else{
                translation = self.translate(input, alphabet: alpha, newAlphabet: rotatedAlpha, delegate: delegate)
            }
            
        })
    }
    
    func randomCipher(input: String, decrypt: Bool, delegate: ViewController) {
        delegate.goButton.enabled = false
//        delegate.settingsButton.enabled = false
        
        if (delegate.past as NSNumber !== 0) {
            for (var i = 0; i < delegate.past; i++) {
                past.removeLast()
            }
        }
        
        if (past.count == 0) {
            past.append(input);
        }
        
        var alpha: Array<String> = getAlphabetArray()
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            var randomAlpha: Array<String> = getRandomAlphabet(alpha, self.key.hashValue)
            
            dispatch_async(dispatch_get_main_queue(), {
                delegate.activity.stopAnimating();
            });
            
            var translation = ""
            
            if !decrypt {
                translation = self.translate(input, alphabet: alpha, newAlphabet: randomAlpha, delegate: delegate)
            }else{
                translation = self.translate(input, alphabet: randomAlpha, newAlphabet: alpha, delegate: delegate)
            }
            
        })
    }
    
    
    func splitKey(input: String, decrypt: Bool, delegate: ViewController) {
        // Split key is a complicated process.
        // Each letter in the string is encrypted by itself, using the first digit of the key's hash...
        // combined with the index of the letter. To do this, it generates a random alphabet for each letter...
        // using that combined number and then translates that character. Then it does it again with the next...
        // digit of the key.
        
        
        // Disable UI
        delegate.goButton.enabled = false
//        delegate.settingsButton.enabled = false
        delegate.cypherSegmented.enabled = false
        delegate.inputTextView.editable = false
        delegate.inputTextView.scrollEnabled = false
        delegate.segmented.enabled = false
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        // Remove pasts from old encryptions
        if (delegate.past as NSNumber !== 0) {
            if (countElements(past) as NSNumber !== 0) {
                for (var i = 0; i < delegate.past; i++) {
                    past.removeLast()
                }
            }
        }
        
        // Put input in only if it is empty
        if (past.count == 0) {
            // Since only the output of encryptions are saved later, the first must also be remembered,
            // without creating any duplicates
            past.append(input);
        }
        
        // Beguin encrypting
        let alpha: Array<String> = getAlphabetArray()
        let alphaDict: Dictionary<String, Int> = getAlphabetDictionary()
        var decrypting: Bool = decrypt
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            // Split up the key
            var splitKey: Array<Int> = []
            let time = NSDate()
            println(self.key.hash)
            println(String("default").hash)
            var keyString = String(self.key.hash)
            var keyArray = Array(keyString)
            
            var part1 = String(keyArray[0...countElements(keyString)/4-1])
            var part2 = String(keyArray[countElements(keyString)/4...countElements(keyString)/4 * 2 - 1])
            var part3 = String(keyArray[countElements(keyString)/4 * 2...countElements(keyString)/4 * 3 - 1])
            var part4 = String(keyArray[countElements(keyString)/4 * 3...countElements(keyString) - 1])
            
            splitKey.append(part1.toInt()!)
            splitKey.append(part2.toInt()!)
            splitKey.append(part3.toInt()!)
            splitKey.append(part4.toInt()!)
            
            if (decrypting) {
                splitKey = splitKey.reverse()
            }
            println("\(splitKey)")
            var output: String = input
            
            var lettersNow = 0;
            var lettersTranslated = 0;
            var progressMode: Bool = defaults.objectForKey("progressMode") as Bool
            println(progressMode)
            
            
            var lettersPerUpdate = (countElements(output) / 30)
            if (countElements(output) > 1000) {
                lettersPerUpdate = lettersPerUpdate * Int(2 * Float(countElements(output))/1000.0)
            }
            if (countElements(output) > 10000) {
                lettersPerUpdate = lettersPerUpdate * Int(2 * Float(countElements(output))/10000.0)
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                delegate.activity.stopAnimating()
            })
            var pass = 1

            var startTime = NSDate();
            var timeInt = 0.0
            var numberOfTimes = 0
            var alphalength:Int = countElements(alpha)
            
            // Start the work
            for digit: Int in splitKey {
                var outcome = ""
                for (var i = 0; i < countElements(output); i++) {
                    // Combine the digit with the
                    numberOfTimes++;
                    startTime = NSDate();
                    
                    var key: Int = (String(abs(digit)) + String(i)).toInt()!
                    
                    
                    var indexOfLetter = 0
                    let letter = "\(output[advance(output.startIndex, i)])"
                    
                    var outIndex = Int()
                    
                    if (!decrypting) {
                        indexOfLetter = alphaDict[letter]!;
                        srand(CUnsignedInt(key));
                        outIndex = (Int(rand())%(alpha.count) + indexOfLetter)%(alpha.count);
                    }else{
                        indexOfLetter = alphaDict[letter]!;
                        srand(CUnsignedInt(key));
                        outIndex = (indexOfLetter - Int(rand())%(alpha.count))%(alpha.count);
                        while (outIndex < 0) {
                            outIndex += alpha.count;
                        }
                    }
                    
                    var cypherLetter = alpha[outIndex]
                    
                    output = String((output as NSString).substringToIndex(i) + cypherLetter + (output as NSString).substringFromIndex(i+1))
                    outcome = outcome + "\(cypherLetter)"
                    
                    if (lettersNow >= lettersPerUpdate && progressMode) {
                        dispatch_async(dispatch_get_main_queue(), {
                            var showText = outcome
                            if (defaults.objectForKey("fullProgressMode") as Bool) {
                                showText = output
                            }
                            delegate.inputTextView.text = showText
                            delegate.progressView.progress = Float(lettersTranslated) / (Float(countElements(output)) * Float(splitKey.count))
                            delegate.progressLabel.hidden = false
                            delegate.progressLabel.text = "Pass \(pass) of \(splitKey.count): \(Int(Float(lettersTranslated) / Float(countElements(input)) * 100)%100)%"
                        });
                        lettersNow = 0
                    }

                    timeInt += -startTime.timeIntervalSinceNow * 1000
                    

                    let avgTime = timeInt / Double(numberOfTimes)
//                    println("T: \(avgTime)")
                    
                    lettersTranslated++
                    lettersNow++
                }
                pass++
            }
            
            self.done(output, delegate: delegate)
        })
    }
    
    func randomEquation(message: String, decrypt: Bool, delegate: ViewController) {
        
    }
    
    func done(output:String, delegate:ViewController) {
        dispatch_async(dispatch_get_main_queue(), {
            delegate.goButton.enabled = true
            delegate.inputTextView.text = output
            delegate.progressView.progress = 0.0
            delegate.progressLabel.hidden = true
            delegate.progressLabel.text = ""
            //                delegate.settingsButton.enabled = true
            
            
            var app = UIApplication.sharedApplication()
            app.idleTimerDisabled = false
            
            delegate.cypherSegmented.enabled = true
            delegate.inputTextView.editable = true
            delegate.inputTextView.scrollEnabled = true
            delegate.segmented.enabled = true
            delegate.settingsButton.enabled = true
            
            let defaults = NSUserDefaults.standardUserDefaults()
            
            if (defaults.objectForKey("autoCopy") as Bool) {
                let pasteboard = UIPasteboard.generalPasteboard()
                pasteboard.string = output
            }
            
            past.append(output)
        });
    }
}