//
//  HelpViewController.swift
//  Cypher
//
//  Created by John Kotz on 8/14/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import Foundation
import UIKit

class HelpViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UITextView!
    var subject = ""
    
    let colorDict: Dictionary<String, UIColor> = dataObject().colorDict
    
    let defaults = NSUserDefaults()
    @IBOutlet weak var DoneButton: UIButton!
    
    override func viewDidLoad() {
        view.backgroundColor = colorDict[String(format: defaults.objectForKey("backgroundColor") as NSString)]
        
        titleLabel.text = titleLabel.text! + self.subject
        title = titleLabel.text
        
        var path = NSBundle.mainBundle().pathForResource("Help", ofType: "plist")
        
        var helpDict: Dictionary = NSDictionary(contentsOfFile: path!)!
        
        println(subject)
        DoneButton.enabled = false
        DoneButton.hidden = true
        
        if (subject == "Algorithms") {
            DoneButton.hidden = false
            DoneButton.enabled = true
        }
        
        let subjectHelp: Array = helpDict[subject]! as NSArray
        
        descriptionLabel.text = "\(subjectHelp[1])"
    }
    
    @IBAction func done(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func setSubject(subject: String) {
        self.subject = subject
    }
}