// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
var alpha = [" ", "!", "#", "$", "%", "&", "(", ")", "*", "+", "-", ".", ",", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "]", "^", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{", "|", "}", "~"]
var alphaDict = [" " : 0, "!" : 1, "#" : 2, "$" : 3, "%" : 4, "&" : 5, "(" : 6, ")" : 7, "*" : 8, "+" : 9, "-" : 10,
    "." : 11, "," : 12, "/" : 13, "0" : 14, "1" : 15, "2" : 16, "3" : 17, "4" : 18, "5" : 19, "6" : 20,
    "7" : 21, "8" : 22, "9" : 23, ":" : 24, ";" : 25, "<" : 26, "=" : 27, ">" : 28, "?" : 29, "@" : 30,
    "A" : 31, "B" : 32, "C" : 33, "D" : 34, "E" : 35, "F" : 36, "G" : 37, "H" : 38, "I" : 39, "J" : 40,
    "K" : 41, "L" : 42, "M" : 43, "N" : 44, "O" : 45, "P" : 46, "Q" : 47, "R" : 48, "S" : 49, "T" : 50,
    "U" : 51, "V" : 52, "W" : 53, "X" : 54, "Y" : 55, "Z" : 56, "[" : 57, "]" : 58, "^" : 59, "`" : 60,
    "a" : 61, "b" : 62, "c" : 63, "d" : 64, "e" : 65, "f" : 66, "g" : 67, "h" : 68, "i" : 69, "j" : 70,
    "k" : 71, "l" : 72, "m" : 73, "n" : 74, "o" : 75, "p" : 76, "q" : 77, "r" : 78, "s" : 79, "t" : 80,
    "u" : 81, "v" : 82, "w" : 83, "x" : 84, "y" : 85, "z" : 86, "{" : 87, "|" : 88, "}" : 89, "~" : 90]
var key = String(str.hash)
var keyArray = Array(key)

var part1 = String(keyArray[0...countElements(key)/2-1]);
var part2 = String(keyArray[countElements(key)/2...countElements(key) - 1]);

let a = part1.toInt()!
let b = part2.toInt()!

srand(UInt32(a))

var inIndex = Int(alphaDict["-"]!)
var outIndex = 0

var x = 0
var remembering: Dictionary<Int,Bool> = Dictionary()
while (x < alpha.count) {
    var value = Int(rand())%(alpha.count)
    while remembering[value] != nil {
        value = (value+1)%(alpha.count)
    }
    if (value == Int(inIndex)) {
        outIndex = value
        break
    }
    remembering[value] = true
    x++
}

outIndex
alpha[outIndex]
