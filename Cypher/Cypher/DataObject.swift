//
//  File.swift
//  Cypher
//
//  Created by John Kotz on 8/8/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import Foundation
import UIKit

class dataObject {
    var colorDict: Dictionary<String, UIColor> = [
        "Grey": UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0),
        "Green": UIColor(red: 0.39, green: 0.65, blue: 0.25, alpha: 1.0),
        "Blue": UIColor(red: 0.29, green: 0.53, blue: 0.91, alpha: 1.0),
        "Light Blue": UIColor(red: 0.09, green: 0.71, blue: 1, alpha: 1.0),
        "Light Green": UIColor(red: 0.67, green: 0.91, blue: 0.11, alpha: 1.0),
        "Orange": UIColor(red: 0.93, green: 0.61, blue: 0.12, alpha: 1.0),
        "Yellow": UIColor(red: 0.95, green: 0.95, blue: 0.08, alpha: 1.0),
        "Brown": UIColor(red: 0.67, green: 0.51, blue: 0.01, alpha: 1.0),
        "Pink": UIColor(red: 0.83, green: 0.22, blue: 0.94, alpha: 1.0),
        "Red": UIColor(red: 0.77, green: 0.27, blue: 0.27, alpha: 1.0),
        "White": UIColor(red: 1, green: 1, blue: 1, alpha: 1.0),
        "Purple": UIColor(red: 0.65, green: 0.42, blue: 0.81, alpha: 1.0),
        "Tan": UIColor(red: 0.83, green: 0.77, blue: 0.59, alpha: 1.0)
    ]
}
