//
//  BackgroundColorChangerViewController.swift
//  Cypher
//
//  Created by John Kotz on 8/4/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import UIKit
import Foundation

class backgroundColorChooser: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let colorKey = "backgroundColor"
    var chosenColor: String = ""
    var chosenColorIndex: Int = 0
    var colors: Array<String> = []
    
    var colorDict: Dictionary<String, UIColor> = dataObject().colorDict
    
    @IBOutlet weak var picker: UIPickerView!
    
    override func viewDidLoad() {
        var defaults = NSUserDefaults.standardUserDefaults()
        
        self.colors = ["Grey", "Green", "Blue", "Light Blue", "Light Green", "Orange", "Yellow", "Brown", "Pink", "Red", "White", "Purple", "Tan"]
        
        if (!contains(defaults.dictionaryRepresentation().keys, colorKey)) {
            defaults.setValue("White", forKey: colorKey)
        }else if (defaults.objectForKey(colorKey) == nil) {
            defaults.setValue("White", forKey: colorKey)
        }
        
        chosenColor = String(format: defaults.objectForKey(colorKey) as String)
        
        for (var i = 0; i < colors.count; i++) {
            var color = colors[i]
            if ((color as NSString).isEqualToString(chosenColor as String)) {
                self.chosenColorIndex = i
                break
            }
        }
        
        picker.selectRow(self.chosenColorIndex, inComponent: 0, animated: false)
        self.updateColor(chosenColor)
        defaults.synchronize()
    }
    
    func updateColor(colorName: String) {
        var color:UIColor = self.colorDict[colorName]!
        
        view.backgroundColor = color
    }
    
    func pickerView(pickerView: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        var colorName = self.colors[row]
        
        var defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(colorName, forKey: colorKey)
        self.updateColor(colorName)
        
        defaults.synchronize()
    }
    
    func pickerView(pickerView: UIPickerView!, titleForRow row: Int, forComponent component: Int) -> String! {
        
        return self.colors[row]
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count;
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1;
    }
}