//
//  ViewController.swift
//  Cypher
//
//  Created by John Kotz on 6/23/14.
//  Copyright (c) 2014 lordtechy. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController, UITextViewDelegate {
    @IBOutlet var inputTextView : UITextView!
    @IBOutlet var goButton : UIButton!
    @IBOutlet var progressView : UIProgressView!
    @IBOutlet var activity : UIActivityIndicatorView!
    @IBOutlet var segmented : UISegmentedControl!
    @IBOutlet var cypherSegmented : UISegmentedControl!
    @IBOutlet var progressLabel : UILabel!
    var isEditing = false
    var encrypt: Encryptor = Encryptor()
    var encrypting = false
    var past = 0
    
    let colorDict: Dictionary<String, UIColor> = dataObject().colorDict
    
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var navBar: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //progressView.progress = 0.0
        encrypt.setup("Barbershop");
        
        var defaults = NSUserDefaults.standardUserDefaults()
        view.backgroundColor = colorDict[String(format: defaults.objectForKey("backgroundColor") as NSString)]
        
        
        if (defaults.objectForKey("shouldAskForPassword") !== nil && defaults.objectForKey("shouldAskForPassword") as Bool){
            if (defaults.objectForKey("password") !== nil && defaults.objectForKey("password") !== ""){
                self.performSegueWithIdentifier("askPassword", sender: nil)
            }
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        var defaults = NSUserDefaults.standardUserDefaults()
        view.backgroundColor = colorDict[String(format: defaults.objectForKey("backgroundColor") as NSString)]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func edit(sender : AnyObject) {
        if !isEditing {
            inputTextView.becomeFirstResponder()
            goButton.titleLabel?.text = "Hide"
            isEditing = true
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        edit(textView)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        inputTextView.resignFirstResponder()
        goButton.titleLabel?.text = "Go"
        isEditing = false
    }
    
    func updateBackground(){
        var defaults = NSUserDefaults.standardUserDefaults()
        view.backgroundColor = colorDict[String(format: defaults.objectForKey("backgroundColor") as NSString)]
    }
    
    @IBAction func goBack(sender : AnyObject) {
        let pastArray: Array<String> = encrypt.getPast()
        if (!(pastArray.count == 0) && !encrypting) {
            if (pastArray.count as NSNumber !== past as NSNumber) {
                past++;
            }
            if (pastArray.count > past) {
                inputTextView.text = pastArray[pastArray.count - 1 - past]
            }
        }
    }
    
    @IBAction func goForward() {
        let pastArray: Array<String> = encrypt.getPast()
        if (!(pastArray.count == 0) && !encrypting) {
            if (past as NSNumber !== 0) {
                past--;
            }
            if (past >= 0) {
                inputTextView.text = pastArray[pastArray.count - 1 - past]
            }
        }
    }
    
    func hide() {
        inputTextView.resignFirstResponder()
        goButton.titleLabel?.text = " Go "
        isEditing = false
    }
    
    @IBAction func touchedOutOfInput(sender: AnyObject) {
        if (isEditing) {
            hide()
        }
    }
    
    @IBAction func help(sender: UIButton) {
        var alert = UIAlertController(title: "Help: Algorithms", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil))
        
        var path = NSBundle.mainBundle().pathForResource("Help", ofType: "plist")
        var helpDict: Dictionary = NSDictionary(contentsOfFile: path!)!
        
        var subjectHelp: Array = helpDict["Algorithms"] as NSArray
        alert.message = "\(subjectHelp[0])"
        
        
        if (subjectHelp.count > 1) {
            alert.addAction(UIAlertAction(title: "More Info", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.performSegueWithIdentifier("moreInfoFromMain", sender: "Algorithms")
            }))
        }
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "moreInfoFromMain") {
            var vc: HelpViewController = segue.destinationViewController as HelpViewController
            vc.setSubject("\(sender)")
        }
    }
    
    @IBAction func goButtonPressed(sender : AnyObject) {
        encrypt.key = NSUserDefaults.standardUserDefaults().objectForKey("key") as NSString
        if isEditing{
            hide()
        }else{
            
            var app = UIApplication.sharedApplication()
            app.idleTimerDisabled = true
            self.activity.startAnimating();
            var decrypting = false
            
            if (segmented.selectedSegmentIndex == 1) {
                decrypting = true;
            }
            if (cypherSegmented.selectedSegmentIndex == 0) {
                encrypt.ceasar(inputTextView.text, decrypt: decrypting, delegate: self)
            }else if (cypherSegmented.selectedSegmentIndex == 1){
                encrypt.randomCipher(inputTextView.text, decrypt: decrypting, delegate: self)
            }else{
                encrypt.splitKey(inputTextView.text, decrypt: decrypting, delegate: self)
            }
        }
    }
}

